<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <title>Space Travel</title>
	<link rel="stylesheet" type="text/css" href="css/main.css"> 
</head>
<body>
	<h1><big>Space Travel</big></h1>
        
        <div>
        <a href="#"> Главная </a>
        <a href="#"> Билеты на Луну </a>
        <a href="#"> Билеты на Марс </a>
        <a href="#"> Туры по Земле </a>
        <a href="#"> Регистрация в SpaceCLub </a>
        <a href="#"> Акции и скидки </a>
        <a href="#"> О нас </a>
 </div>
 <hr>
<h2><big> Future is here.</big> </h2>
<br>
<h3><big> Космотуризм </big></h3>
    <p><big> Объездили всю планету Земля?  Хочется новых впечатлений? На Земле стало скучно и хочется увидеть что-то новое? <br>
	Теперь это легко исправить. <br>
	Благодаря трудам команды Илона Маска по изучению космоса и его стремлению расширить границы для человечества, открыты быстрые рейсы на Луну и Марс. <br>
    Наша компания существует с 2047 и является лучшей в галактике по предоставлению услуг космотуризма. <br> <br>
    Для тех, кто не может улететь с планеты Земля по состоянию здоровья - предлагаются высокоскоростные порталы, которые за секунды переместят Вас с одного конца Земли на другой. <br> <br>
    </big></p>
    


 <marquee direction="right" bgcolor="#0000FF">Never stop exploring</marquee>
 <br>
 <br>

 <h2><big>Выбор путешествия</big></h2>
 <br>

 <div #img>
      
 	<img src="img/moon.jpg" alt="Луна" title="Луна" width="491" height="491">
 <img src="https://lh6.googleusercontent.com/proxy/1rTeqD__TwgsssdlXqn7toIbJnEBlbjUNZUUQmJrk3CryP7sO_Plyn0_K2BPdcsrsuaSQ_eB8qXfcS-n8k5WD2F_XIWSFX-sFg"  width="491" height="491" title="Марс">
<img src="https://aboutspacejornal.net/wp-content/uploads/2016/01/800px-Earth_Eastern_Hemisphere1-e14533000763171-640x640.jpg" height="491" title="Земля">

</div>

<h2><big>Направления высокоскоростных порталов по Земле</big></h2>
 <br>
     <ul type="disk">
     <li>Лондон-Нью Йорк:3000$</li> 
     <li>Мадрид-Париж:3000$</li>
     <li>Лондон-Дублин:3000$</li>
     <li>Амстердам-Лондон:3000$</li>
     <li>Париж-Нью Йорк:3000$</li>
     <li>Париж-Дубай:3000$</li>
     <li>Лондон-Чикаго:3000$</li>
     <li>Гонконг-Лондон:3000$</li>
     <li>Монреаль-Париж:3000$</li>
 </ul>

     <h2><big>Рейсы на Луну 2064 год</big></h2>
     <br>
     
     <ul type="disk">
     <li>Январь - нет сводных рейсов</li> 
     <li>Февраль - 7 </li>
     <li>Март - 23</li>
     <li>Апрель 8</li>
     <li>Май - 1, 26</li>
     <li>Июнь - нет свободных рейсов</li>
     <li>Июль - нет свободных рейсов</li>
 </ul>

     <br>
     
<h2><big>Рейсы на Марс 2064 год</big></h2>
     <br>
     
     <ul type="disk">
     <li>Январь - нет сводных рейсов</li> 
     <li>Февраль - нет свободных рейсов</li>
     <li>Март - 16, 25</li>
     <li>Апрель 2,21</li>
     <li>Май - 15</li>
     <li>Июнь - 2, 19</li>
     <li>Июль - информация о рейсах недоступна</li>
   </ul>
   <br><br>

   <a id="connect" href="#"> Связаться с менеджером.


hi

   



</body>
</html>